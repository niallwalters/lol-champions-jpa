package com.traning.springexercise.controller;

import com.traning.springexercise.entities.LolChampion;
import com.traning.springexercise.service.LolChampionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("api/v1/lolchampion")
public class LolChampionController {

    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return lolChampionService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<LolChampion> findById(@PathVariable long id) {
        try {
            return new ResponseEntity<LolChampion>(lolChampionService.findById(id), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            // return 404
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity<LolChampion> create(@RequestBody LolChampion lolChampion){
        return new ResponseEntity<LolChampion>(lolChampionService.save(lolChampion), HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<LolChampion> update(@RequestBody LolChampion lolChampion) {
        try {
            return new ResponseEntity<>(lolChampionService.update(lolChampion), HttpStatus.OK);
        } catch(NoSuchElementException ex) {
            LOG.debug("update for unknown id: [" + lolChampion + "]");
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable long id) {
        try {
            lolChampionService.delete(id);
        } catch(EmptyResultDataAccessException ex) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
