package com.traning.springexercise.service;

import com.traning.springexercise.entities.LolChampion;
import com.traning.springexercise.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    @Autowired
    LolChampionRepository lolChampionRepository;

    public List<LolChampion> findAll(){
        return lolChampionRepository.findAll();
    }

    public LolChampion findById(long id) {
        return lolChampionRepository.findById(id).get();
    }

    public LolChampion save(LolChampion lolChampion){
        return  lolChampionRepository.save(lolChampion);
    }

    public LolChampion update(LolChampion lolChampion) {
        lolChampionRepository.findById(lolChampion.getId()).get();

        return lolChampionRepository.save(lolChampion);
    }

    public void delete(long id) {
        lolChampionRepository.deleteById(id);
    }
}
