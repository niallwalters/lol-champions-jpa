package com.traning.springexercise.repository;

import com.traning.springexercise.entities.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

@Component
public interface LolChampionRepository extends JpaRepository <LolChampion, Long> {

}
